package com.ganesh.rps.domain;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ganesh.rps.service.RPSGameSetup;

public class RPSGameSetupTest {

	private RPSGameSetup gameSetup;

	@Before
	public void setup() {
		gameSetup = new RPSGameSetup();
	}

	@Test
	public void shouldWinOverPeoperly() {
		assertEquals(Result.LOST,gameSetup.ROCK.canWinOver(gameSetup.PAPER));
		assertEquals(Result.WON,gameSetup.PAPER.canWinOver(gameSetup.ROCK));
		assertEquals(Result.DRAW,gameSetup.PAPER.canWinOver(gameSetup.PAPER));
		
	}

	
	@Test
	public void shouldInitialiseSigns(){
		assertEquals(3, gameSetup.getGameSigns().size());
	}
	
	@Test
	public void shouldRetrieveSignBasedOnName(){
		assertEquals(gameSetup.ROCK, gameSetup.fromName("rock"));
	}

}
