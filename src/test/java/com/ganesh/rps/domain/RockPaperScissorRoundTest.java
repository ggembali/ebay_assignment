package com.ganesh.rps.domain;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.ganesh.rps.service.RPSGameSetup;


public class RockPaperScissorRoundTest {
	RockPaperScissorRound rockPaperScissorRound;
	
	@Before
	public void setup(){
		rockPaperScissorRound  = new RockPaperScissorRound(0,2);
	}

	@Test
	public void shouldDetermineIfRoundIsover() throws UserSignAlreadyAddedOrRoundOverException{
		rockPaperScissorRound.addUserSign(new User("dummy", "Dummy"), new RPSGameSetup().ROCK);
		assertFalse("With 1 player submitting hise/her sign, round is not over",rockPaperScissorRound.isRoundOver());
		
		rockPaperScissorRound.addUserSign(new User("dummy2", "Dummy 2"), new RPSGameSetup().PAPER);
		assertTrue("With 2 players submitting their sign, round is over",rockPaperScissorRound.isRoundOver());
		
	}
	
	@Test(expected=UserSignAlreadyAddedOrRoundOverException.class)
	public void shouldNotAllowSameUserSecondTime() throws UserSignAlreadyAddedOrRoundOverException{
		rockPaperScissorRound.addUserSign(new User("dummy", "Dummy"), new RPSGameSetup().ROCK);
		rockPaperScissorRound.addUserSign(new User("dummy", "Dummy"), new RPSGameSetup().ROCK);
	}

	@Test(expected=RoundNotOverException.class)
	public void shouldNotDetermineWinnerTillAllUsersShowSign() throws UserSignAlreadyAddedOrRoundOverException, RoundNotOverException{
		rockPaperScissorRound.addUserSign(new User("dummy", "Dummy"), new RPSGameSetup().ROCK);
		rockPaperScissorRound.getWinner();
	}
	
	@Test
	public void shouldReturnNullIfDraw() throws UserSignAlreadyAddedOrRoundOverException, RoundNotOverException{
		rockPaperScissorRound.addUserSign(new User("dummy1", "Dummy"), new RPSGameSetup().ROCK);
		rockPaperScissorRound.addUserSign(new User("dummy2", "Dummy"), new RPSGameSetup().ROCK);
		
		assertNull(rockPaperScissorRound.getWinner());
		
	}
	
	@Test
	public void shouldDetermineWinner() throws UserSignAlreadyAddedOrRoundOverException, RoundNotOverException{
		User winner = new User("dummy2", "Dummy");
		rockPaperScissorRound.addUserSign(winner, new RPSGameSetup().PAPER);
		rockPaperScissorRound.addUserSign(new User("dummy", "Dummy"), new RPSGameSetup().ROCK);
		assertEquals(winner,rockPaperScissorRound.getWinner());
	}

}
