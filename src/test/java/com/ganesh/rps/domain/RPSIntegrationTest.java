package com.ganesh.rps.domain;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.ganesh.rps.controller.RockPaperScissorGameController;
import com.ganesh.rps.dao.UserDAO;
import com.ganesh.rps.dao.UserDAOImpl;
import com.ganesh.rps.service.RPSGameSetup;
import com.ganesh.service.RPSSessionServiceImpl;

public class RPSIntegrationTest {

	RPSSessionServiceImpl rpsSessionService = new RPSSessionServiceImpl();
	UserDAO userDAO = new UserDAOImpl();
	RPSGameSetup rpsGameSetup = new RPSGameSetup();

	RockPaperScissorGameController rockPaperScissorGameController;
	
	@Before
	public void setup(){
		rpsSessionService.setRpsGameSetup(rpsGameSetup);
		rpsSessionService.setUserDAO(userDAO);
		
		
		rockPaperScissorGameController = new RockPaperScissorGameController();
		rockPaperScissorGameController.setRpsGameSetup(rpsGameSetup);
		rockPaperScissorGameController.setRpsSessionService(rpsSessionService);
		rockPaperScissorGameController.setUserDAO(userDAO);
	}
	
	@Test
	public void shouldStartANewSession() throws UserSignAlreadyAddedOrRoundOverException{
		String username = "ggembali";
		HashMap<String,Object> startNewGame = rockPaperScissorGameController.startNewGame(username, "GaneshGembali", SessionType.PlayerVsComputer.name());
		String sessionId = ((String) startNewGame.get("sessionId"));
		rockPaperScissorGameController.castSign(sessionId, username, rpsGameSetup.ROCK.getName());
		rockPaperScissorGameController.castSign(sessionId, username, rpsGameSetup.SCISSORS.getName());
		rockPaperScissorGameController.castSign(sessionId, username, rpsGameSetup.PAPER.getName());
		GameResultsVO prepareResults = rpsSessionService.getSession(sessionId).prepareResults();
	}
}
