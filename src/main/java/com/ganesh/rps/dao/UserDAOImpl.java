package com.ganesh.rps.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.ganesh.rps.domain.User;
import com.ganesh.rps.domain.UserAlreadyExistsException;

@Service
public class UserDAOImpl implements UserDAO {

	private Map<String,User> userDB = new HashMap<String, User>();
	
	public void addUser(User user) throws UserAlreadyExistsException {
		if(userDB.containsKey(user.getUsername())){
			throw new UserAlreadyExistsException();
		}
		userDB.put(user.getUsername(), user);
	}

	public Collection<User> getAllUsers() {
		return userDB.values();
	}

	public void removeUser(String username) {
		userDB.remove(username);
	}

	public User getUser(String username) {
		return userDB.get(username);
	}

}
