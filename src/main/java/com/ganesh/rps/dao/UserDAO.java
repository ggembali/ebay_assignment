package com.ganesh.rps.dao;

import java.util.Collection;

import com.ganesh.rps.domain.User;
import com.ganesh.rps.domain.UserAlreadyExistsException;


public interface UserDAO {
	
	public void addUser(User user) throws UserAlreadyExistsException;
	public Collection<User> getAllUsers();
	public void removeUser(String username);
	public User getUser(String username);
}
