package com.ganesh.rps.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ganesh.rps.dao.UserDAO;
import com.ganesh.rps.domain.GameSession;
import com.ganesh.rps.domain.SessionType;
import com.ganesh.rps.domain.User;
import com.ganesh.rps.domain.UserAlreadyExistsException;
import com.ganesh.rps.domain.UserSignAlreadyAddedOrRoundOverException;
import com.ganesh.rps.service.RPSGameSetup;
import com.ganesh.service.RPSSessionService;

@Controller
public class RockPaperScissorGameController {

	public void setRpsSessionService(RPSSessionService rpsSessionService) {
		this.rpsSessionService = rpsSessionService;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public void setRpsGameSetup(RPSGameSetup rpsGameSetup) {
		this.rpsGameSetup = rpsGameSetup;
	}

	@Autowired
	private RPSSessionService rpsSessionService;

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private RPSGameSetup rpsGameSetup;

	@RequestMapping("/index")
	@ResponseBody 
	public String startPage(Map<String, Object> map) {
		return "contact";
	}

	@RequestMapping(value = "/new/{username}/{fullName}/{sessionType}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String,Object> startNewGame(@PathVariable("username") String username,
			@PathVariable("fullName") String fullName,
			@PathVariable("sessionType") String sessionType) {

		User user = new User(username, fullName);
		try {
			userDAO.addUser(user);
		} catch (UserAlreadyExistsException e) {
			return null;
		}
		GameSession session = rpsSessionService.createNewGameSession(user,
				SessionType.valueOf(sessionType));
		HashMap<String, Object> response = new HashMap<String, Object>();
		response.put("sessionId", session.getSessionId());
		response.put("signs", rpsGameSetup.getGameSigns());
		
		System.out.println(session.getSessionId());
		return response;
	}

	@RequestMapping("/game/{sessionId}/{username}/{handsign}")
	public @ResponseBody GameSession castSign(@PathVariable("sessionId") String sessionId,
								@PathVariable("username") String username,
								@PathVariable("handsign") String handsign)
								throws UserSignAlreadyAddedOrRoundOverException {
		
		GameSession session = rpsSessionService.getSession(sessionId);
		session.addUserHandsign(userDAO.getUser(username),rpsGameSetup.fromName(handsign));

		HashMap<String, Object> response = new HashMap<String, Object>();
		response.put("sessionId", session.getSessionId());
		response.put("signsByUsers", session.getRounds().get(session.getCurrentRoundNumber()-1));

		return session;
	}
}
