package com.ganesh.rps.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ganesh.rps.domain.HandSign;

@Service
public class RPSGameSetup {

	public final HandSign ROCK = new HandSign("rock");
	public final HandSign PAPER = new HandSign("paper");
	public final HandSign SCISSORS = new HandSign("scissors");

	
	private List<HandSign> allSigns;

	public RPSGameSetup() {
		ROCK.setWinsOver(Arrays.asList(SCISSORS));
		PAPER.setWinsOver(Arrays.asList(ROCK));
		SCISSORS.setWinsOver(Arrays.asList(PAPER));
		allSigns = Arrays.asList(ROCK, PAPER, SCISSORS);
	}

	public  List<HandSign> getGameSigns() {
		return allSigns;
	}

	public HandSign fromName(String handsign) {
		for(HandSign sign : allSigns){
			if(sign.getName().equals(handsign)){
				return sign;
			}
		}
		return null;
	}

}
