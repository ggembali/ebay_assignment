package com.ganesh.rps.domain;

import java.util.List;
import java.util.Random;

import com.ganesh.rps.service.RPSGameSetup;

public class SystemUser extends User {
	
	RPSGameSetup gameSetup;
	
	
	public SystemUser(String systemUserName, RPSGameSetup rpsGameSetup) {
		super(systemUserName,"RockPaperScissor AutoBot");
		this.gameSetup = rpsGameSetup;
	}
	
	public HandSign getTheNextRandomSign(){
		List<HandSign> allSigns = gameSetup.getGameSigns();
		int nextInt = new Random().nextInt(allSigns.size());
		return allSigns.get(nextInt);
	}
	
}
