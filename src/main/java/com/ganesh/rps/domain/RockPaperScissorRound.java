package com.ganesh.rps.domain;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RockPaperScissorRound {
	private int roundNumber;
	private Map<User,HandSign> handsignsByUsers = new HashMap<User, HandSign>();
	private final int numberOfPartiscipants;
	
	public RockPaperScissorRound(int roundNumber,int numberOfPartiscipants) {
		this.roundNumber = roundNumber;
		this.numberOfPartiscipants = numberOfPartiscipants;
	}
	
	public void addUserSign(User user,HandSign sign) throws UserSignAlreadyAddedOrRoundOverException{
		if(handsignsByUsers.containsKey(user) || isRoundOver()){
			throw new UserSignAlreadyAddedOrRoundOverException();
		}
		handsignsByUsers.put(user, sign);
	}
	
	public int getRoundNumber() {
		return roundNumber;
	}
	
	public User getWinner() throws RoundNotOverException{
		if(!isRoundOver()){
			throw new RoundNotOverException();
		}
		
		Iterator<User> users = handsignsByUsers.keySet().iterator();
		
		User user1 = users.next();
		User user2 = users.next();
		Result canWinOver = handsignsByUsers.get(user1).canWinOver(handsignsByUsers.get(user2));
		if(canWinOver.equals(Result.DRAW)){
			return null;
		}
		return canWinOver.equals(Result.WON) ? user1 : user2;
		
	}

	public Map<User, HandSign> getHandsignsByUsers() {
		return handsignsByUsers;
	}
	public boolean isRoundOver() {
		return handsignsByUsers.size() == numberOfPartiscipants;
	}
}
