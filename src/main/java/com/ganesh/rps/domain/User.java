package com.ganesh.rps.domain;

public class User {
	private String username;
	private String fullName;

	public User(String username, String fullName) {
		super();
		this.username = username;
		this.fullName = fullName;
	}

	public String getUsername() {
		return username;
	}

	public String getFullName() {
		return fullName;
	}

	@Override
	public int hashCode() {
		return this.username.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.username.equals(((User)obj).getUsername());
	}
	
	@Override
	public String toString() {
		return getUsername();
	}
}
