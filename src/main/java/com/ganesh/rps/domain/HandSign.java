package com.ganesh.rps.domain;

import java.util.List;

public class HandSign {
	private String name;
	private List<HandSign> winsOver;
	
	public HandSign(String name) {
		this.name = name;
	}

	public Result canWinOver(HandSign sign){
		if(this.equals(sign)){
			return Result.DRAW;
		}
		return winsOver.contains(sign)?Result.WON : Result.LOST;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWinsOver(List<HandSign> winsOver) {
		this.winsOver = winsOver;
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.name.equals(((HandSign)obj).getName());
	}
	@Override
	public String toString() {
		return getName();
	}
}
