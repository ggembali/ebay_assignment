package com.ganesh.rps.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public abstract class GameSession {
	private List<User> partiscipants;
	private int maxRounds;
	private List<RockPaperScissorRound> rounds;
	private RockPaperScissorRound currentRound;
	private int currentRoundNumber = 0;
	private Map<User, Integer> scorecard = new HashMap<User, Integer>();

	private String sessionId;

	public GameSession(List<User> partiscipants) {
		this.partiscipants = partiscipants;
		maxRounds = 3;
		rounds = new ArrayList<RockPaperScissorRound>();
		currentRound = new RockPaperScissorRound(0, partiscipants.size());
		sessionId = UUID.randomUUID().toString();
		initScorecard();
	}

	private void initScorecard() {
		for (User user : partiscipants) {
			scorecard.put(user, 0);
		}
	}

	public abstract void addUserHandsign(User user, HandSign sign)
			throws UserSignAlreadyAddedOrRoundOverException;

	public RockPaperScissorRound getCurrentRound() {
		return currentRound;
	}

	public void setCurrentRound(RockPaperScissorRound currentRound) {
		if (this.currentRound != currentRound) {
			currentRoundNumber++;
			rounds.add(currentRound);
		}
		this.currentRound = currentRound;
	}

	public GameResultsVO prepareResults() {
		return new GameResultsVO(scorecard);
	}

	protected void incrementWinnerScore(){
		User winner = null;
		try {
			winner = currentRound.getWinner();
		} catch (RoundNotOverException e) {
			// TODO Auto-generated catch block
		}
		if (winner != null) {
			scorecard.put(winner, scorecard.get(winner) + 1);
		}
	}

	public boolean isGameSessionCompleted() {
		return maxRounds == currentRoundNumber && currentRound.isRoundOver();
	}

	public int getCurrentRoundNumber() {
		return currentRoundNumber;
	}

	public List<User> getPartiscipants() {
		return partiscipants;
	}

	public int getMaxRounds() {
		return maxRounds;
	}

	public List<RockPaperScissorRound> getRounds() {
		return rounds;
	}

	public String getSessionId() {
		return sessionId;
	}

}
