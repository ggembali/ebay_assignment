package com.ganesh.rps.domain;

import java.util.Iterator;
import java.util.Map;

public class GameResultsVO {

	private User winner; // :)

	private User loser; // :(

	
	private Map<User, Integer> scorecard;

	
	public GameResultsVO(Map<User, Integer> scorecard) {
		this.scorecard = scorecard;
		initWinnerLoser(scorecard);

	}
	

	//Needs to be overriden when more than two players play
	protected void initWinnerLoser(Map<User, Integer> scorecard) {
		Iterator<User> partiscipants = scorecard.keySet().iterator();
		User user1 = partiscipants.next();
		User user2 = partiscipants.next();
		
		if(scorecard.get(user1) == scorecard.get(user2) ){
			return;
		}
			
		winner = scorecard.get(user1) > scorecard.get(user2) ? user1 : user2;
		loser = scorecard.get(user1) > scorecard.get(user2) ? user2 : user1;
	}

	public User getWinner() {
		return winner;
	}

	public User getLoser() {
		return loser;
	}

	public int getTotalWinsByWinner() {
		if(winner == null){
			return 0;
		}
		return scorecard.get(winner);
	}

}
