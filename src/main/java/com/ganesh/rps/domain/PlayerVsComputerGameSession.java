package com.ganesh.rps.domain;

import java.util.ArrayList;
import java.util.List;

public class PlayerVsComputerGameSession extends GameSession {

	private List<SystemUser> systemUsers = new ArrayList<SystemUser>();

	public PlayerVsComputerGameSession(List<User> partiscipants) {
		super(partiscipants);
		filterSystemUsers(partiscipants);
		setCurrentRound(new RockPaperScissorRound(getCurrentRoundNumber() + 1,
				getPartiscipants().size()));
	}

	private void filterSystemUsers(List<User> partiscipants) {
		for (User user : partiscipants) {
			if(user instanceof SystemUser){
				systemUsers.add((SystemUser) user);
			}
		}
	}

	@Override
	public void addUserHandsign(User user, HandSign sign)
			throws UserSignAlreadyAddedOrRoundOverException {
		RockPaperScissorRound currentRound = getCurrentRound();
		currentRound.addUserSign(user, sign);
		for (SystemUser systemUser : systemUsers) {
			HandSign theNextRandomSign = systemUser.getTheNextRandomSign();
			currentRound.addUserSign(systemUser, theNextRandomSign);
			System.out.println(theNextRandomSign+ "  "+sign.getName());
		}
		if (currentRound.isRoundOver()) {
			incrementWinnerScore();
			setCurrentRound(new RockPaperScissorRound(getCurrentRoundNumber() + 1, getPartiscipants().size()));
		}
	}

}
