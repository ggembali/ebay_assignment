package com.ganesh.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ganesh.rps.dao.UserDAO;
import com.ganesh.rps.domain.ComputeVsComputerGameSession;
import com.ganesh.rps.domain.GameSession;
import com.ganesh.rps.domain.PlayerVsComputerGameSession;
import com.ganesh.rps.domain.SessionType;
import com.ganesh.rps.domain.SystemUser;
import com.ganesh.rps.domain.User;
import com.ganesh.rps.service.RPSGameSetup;


@Service
public class RPSSessionServiceImpl implements RPSSessionService {


	@Autowired
	private UserDAO userDAO;

	@Autowired
	private RPSGameSetup rpsGameSetup;

	private Map<String, GameSession> sessions = new HashMap<String, GameSession>();
	
	public GameSession createNewGameSession(User user, SessionType sessionType) {
		GameSession session;
		if(sessionType.equals(SessionType.PlayerVsComputer)){
			List<User> users = new ArrayList<User>();
			users.add(user);
			users.add(createNewSysemUser("System User"));
			session = new PlayerVsComputerGameSession(users);
		}else {
			User systemUser1 = createNewSysemUser("System User1");
			User systemUser2 = createNewSysemUser("System User2");
			session = new ComputeVsComputerGameSession(Arrays.asList(systemUser1,systemUser2));
		}
		
		sessions.put(session.getSessionId(), session);
		return session;
	}

	public SystemUser createNewSysemUser(String systemUserName){
		return new SystemUser(systemUserName,rpsGameSetup);
	}
	public GameSession getSession(String sessionId) {
		return sessions.get(sessionId);
	}
	
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	public void setRpsGameSetup(RPSGameSetup rpsGameSetup) {
		this.rpsGameSetup = rpsGameSetup;
	}
	

}
