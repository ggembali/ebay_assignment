package com.ganesh.service;

import com.ganesh.rps.domain.GameSession;
import com.ganesh.rps.domain.SessionType;
import com.ganesh.rps.domain.User;



public interface RPSSessionService {
	public GameSession createNewGameSession(User user, SessionType sessionType);
	
	public GameSession getSession(String sessionId);

}
